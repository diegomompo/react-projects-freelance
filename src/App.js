import logo from './logo.svg';
import './App.css';
import Saludar from "./components/Saludar";
import React , {useState} from "react";

function App() {

    const [stateCar, setStateCar] = useState(false);
    const user = {
        nombre: "Diego Mompó",
        edad : 22,
        color : "Rojo"
    };

    const saludarFN = (name, edad, color) =>{
        console.log("hola " + name + " tiene " + edad + " años")
        console.log(`Hola ${name}, tiene ${edad} años `)
    }

    const encenderApagar = () => {
        setStateCar(prevValue => !prevValue)
    }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
          <Saludar name="salu" userInfo={user} saludarFN={saludarFN}/>
          <h3>EL coche esta: {stateCar ? "Encendido" : "Apagado"}</h3>
          <button onClick={encenderApagar}>Encender / Apagar</button>
      </header>
    </div>
  );
}

export default App;

